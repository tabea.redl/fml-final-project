# FML Final Project 

This repository contains all code that was used for the final project created during the class of Foundation of Machine Learning of the Master DSBA at CentraleSupélec.

We trained a CNN and different variations of autoencoders. They can be found in their respective folders. 

The dataset that we used is publicly available under the following link: https://www.mvtec.com/company/research/datasets/mvtec-ad

All other details of the project can be found in the project report. 
